## Semapy
A simple python client for semaphore


## Installation

It is not on PyPi yet so install directly from the repo instead:

```bash
# shell
pip install git+https://gitlab.com/aapjeisbaas/semapy.git
```

or 

```txt
# requiremetns.txt
git+https://gitlab.com/aapjeisbaas/semapy.git
```

## Usage

A basic import and init the client.

```py
import semapy

semaphore_client = semapy.Client(
    username = 'admin',
    password = 'admin',
    url = 'http://127.0.0.1:3000/api'
)


# Check if connection is successful
info = semaphore_client.info()
if 'version' in info:
    print(f"Connection successful! \nSemaphore version: {info['version']} \n{info['ansible']}")
else:
    print("Connection failed")
    exit(1)

```

## Support

Open an issue on gitlab or try to fix it and send a merge request.

## Roadmap

I strive to add all the basics to create a minimal setup in semaphore, I'll probably not add every feature or edge case.

## Contributing

Contributions are welcome except if it doesn't stay **simple** to use ;-)


## License

BSD;  do whatever you want with it, I'm not liable for the correctness of the code
