"""
Core client functionality, common across all API requests.
"""

import requests
from urllib.parse import urlencode
import json
from pprint import pprint

_USER_AGENT = 'semaphore.py'

class Client(object):
    """Performs requests to the semaphore API."""

    def __init__(self, username, password, url='https://demo.ansible-semaphore.com/api'):
        """Base semaphore api client."""

        self._base_url = url
        self.username = username
        self.password = password
        self.session = requests.Session()
        self._login()

    def _login(self):
        """Log in to the api by retrieving a Bearer token"""

        # drop all cookies and headers to prevent 2nd login failure due to timed out tokens
        self.session.cookies.clear()
        self.session.headers.clear()

        self.session.headers.update({
            'User-Agent': _USER_AGENT,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        })

        payload = json.dumps({
            'auth': self.username,
            'password': self.password
        })

        r = self.session.post(
            url = f'{self._base_url}/auth/login',
            data = payload
        )


    def _post(self, url, payload=None):
        """Performs HTTP POST with credentials, returning the body as JSON."""

        response = self.session.post(url, data=payload)
        if response.status_code == 401:
            self._login()
            response = self.session.post(url, data=payload)
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return response

    def _put(self, url, payload=None):
        """Performs HTTP PUT with credentials, returning the body as JSON."""

        response = self.session.put(url, data=payload)
        if response.status_code == 401:
            self._login()
            response = self.session.put(url, data=payload)
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return response

    def _get(self, url):
        """Performs HTTP GET with credentials, returning the body as JSON."""

        response = self.session.get(url)
        if response.status_code == 401:
            self._login()
            response = self.session.get(url)
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return response

    def  info(self):
        """Fetches information about semaphore"""

        return self._get(self._base_url + "/info")

    def projects(self):
        """Fetches all the semaphore projects"""
        
        return self._get(self._base_url + "/projects")

    def create_project(self, name):
        """Creates a new semaphore project"""

        return self._post(self._base_url + "/projects", json.dumps({"name": name, "alert": False}))

    def keys(self, project):
        
        return self._get(self._base_url + f"/project/{project}/keys")

    def create_key(self, project, body):

        # api wants the project id in the creation body and in the url
        body['project_id'] = project

        return self._post(self._base_url + f"/project/{project}/keys", json.dumps(body))

    def repositories(self, project):
        
        return self._get(self._base_url + f"/project/{project}/repositories")

    def create_repository(self, project, body):

        # api wants the project id in the creation body and in the url
        body['project_id'] = project

        # fetch keys and match name to get the id
        keys = self.keys(project)
        key_id = [x['id'] for x in keys if x['name'] == body['access_key_name']][0]
        body['ssh_key_id'] = key_id

        # remove access_key from body
        del body['access_key_name']

        return self._post(self._base_url + f"/project/{project}/repositories", json.dumps(body))